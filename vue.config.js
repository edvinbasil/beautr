module.exports = {
  productionSourceMap: false,
  devServer: {
    port: 8081,
    proxy: {
      "/api": {
        target: "http://webp.com",
        logLevel: "debug"
      }
    }
  }
};
