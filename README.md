<h1 align="center">Beautr</h1>
<p>
  <a href="https://gitlab.com/nikhilnathr/beauty-salon-management">
    <img alt="Documentation" src="https://img.shields.io/badge/backend-gitlab-brightgreen" target="_blank" />
  </a>
  <a href="https://www.gnu.org/licenses/gpl-3.0.txt">
    <img alt="License: GPLv3" src="https://img.shields.io/badge/License-GPLv3-yellow.svg" target="_blank" />
  </a>
</p>

> Beauty salon management system: Frontend repo

### [Live version: beautr.me](https://beautr.me)
### [Backend Repo](https://gitlab.com/nikhilnathr/beauty-salon-management)

## Features
### Customer
- [x] See profile
- [x] See available services with pricing
- [x] See staff availability for a service for a time
- [x] See experience for staff
- [x] Create booking for available staff
- [x] View previous bookings
- [x] Rate bookings that are completed
### Staff
- [x] View profile, salary and experience points
- [x] Show bookings made by customers
- [x] Mark booking as complete
- [x] Show rating given by customer
### Admin
- [x] All functions that a staff can do (View bookings, mark complete etc)
- [x] Create Staff
- [x] View Staffs
- [x] Edit Staff
- [x] Delete Staff
- [x] Create Service
- [x] View Services
- [x] Edit Service
- [x] Delete Service

## Development Instructions

## Install Dependencies

```sh
npm install
```

## Usage/Serving locally

```sh
npm run serve
```

## Build for production & deploy

```sh
npm run build
```

## Authors

👤 **Edvin Basil Samuval**  
👤 **Nikhil Nath R**  
👤 **Alan Thomas**


## 📝 License

This project is [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt) licensed.

***