import Vue from "vue";
import axios from "axios";
import VuePromiseBtn from "vue-promise-btn";
import "vue-promise-btn/dist/vue-promise-btn.css";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import "@/assets/styles/main.css";

axios.defaults.baseURL = "/api/";

Vue.config.productionTip = false;
Vue.use(VuePromiseBtn);

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
