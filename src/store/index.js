import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loggedIn: false,
    user: {},
    requesterr: "",
    bookings: []
    // services: [],
    // currService: ""
  },
  getters: {
    loggedIn: state => {
      // console.log(`
      // state.loggedIn: ${state.loggedIn}
      // localStorage.getItem("loggedIn"): ${localStorage.getItem("loggedIn")}
      //   `);
      const a = state.loggedIn || localStorage.getItem("loggedIn");
      const b = !a || a === "false" ? false : a;
      console.log("LOGGEDINSTATE:::", b);
      return b;

      // const ls = localStorage.getItem("loggedIn");
      // console.log(`
      // state.loggedIn: ${state.loggedIn}
      // localStorage.getItem("loggedIn"): ${ls}
      //   `);
      // const a = !ls || ls === "false" ? false : ls;
      // return a;
    },
    userFullName: state => `${state.user.firstname} ${state.user.lastname}`,
    userGender: state => {
      let g = state.user.gender;
      return g === "m" ? "male" : g === "f" ? "female" : "human";
    }
  },

  mutations: {
    SETSELF(state, payload) {
      state.user = payload.data;
    },
    SETLOGGEDIN(state, payload) {
      state.loggedIn = payload;
      localStorage.setItem("loggedIn", payload);
    },
    SETREQERR(state, payload) {
      if (payload) {
        state.requesterr =
          payload.response.data.response.message || payload.message;
        setTimeout(() => {
          state.requesterr = "";
        }, 1200);
      } else {
        state.requesterr = "";
      }
    },
    SETBOOKINGS(state, payload) {
      if (payload) {
        state.bookings.push(payload);
      }
    }
    // SETSERVICES(state, payload) {
    //   state.services = payload.data;
    // },
    // SETCURRSERVICE(state, payload) {
    //   state.currService = payload.data;
    // }
  },
  actions: {
    LOGIN({ commit }, payload) {
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        return axios
          .post("login.php", payload)
          .then(({ data }) => {
            commit("SETLOGGEDIN", payload.type);
            console.log("Logged in\nRecieved data:");
            console.log(data);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    SIGNUP({ commit }, payload) {
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        return axios
          .post("customer.php", payload)
          .then(({ data }) => {
            console.log("Signed up\nRecieved data:");
            console.log(data);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    LOGOUT({ commit }) {
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        return axios
          .get("logout.php")
          .then(({ data }) => {
            commit("SETLOGGEDIN", false);
            console.log("Logged Out");
            console.log(data);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    GETSELF({ commit, getters }) {
      return new Promise((resolve, reject) => {
        const loggedInState = getters.loggedIn;
        if (!["staff", "customer"].includes(loggedInState)) {
          const err = new Error("UNK_STATE");
          err.message = "Unknown state";
          commit("SETREQERR", err);
          return reject(err);
        }
        const url = `${loggedInState}.php`;
        commit("SETREQERR", "");
        return axios
          .get(url)
          .then(({ data, status }) => {
            commit("SETSELF", data);
            commit("SETLOGGEDIN", data.data.type);
            console.log("Got Self with status " + status);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    getAllService({ commit }) {
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        return axios
          .get("service.php")
          .then(({ data, status }) => {
            // commit("SETSERVICES", data);
            console.log("Got services with status " + status);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    getOneService({ commit }, payload) {
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        return axios
          .get(`service.php?id=${payload.id}&time=${payload.time}`)
          .then(({ data, status }) => {
            // commit("SETCURRSERVICE", data);
            console.log("Got service with status " + status);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    getAllStaffs({ commit }) {
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        return axios
          .get("staff.php?all=true")
          .then(({ data, status }) => {
            // commit("SETSERVICES", data);
            console.log("Got staffs with status " + status);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    deleteEntity({ commit }, payload) {
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        const { entity, id } = payload;
        return axios
          .delete(`${entity}.php?id=${id}`)
          .then(({ data, status }) => {
            // commit("SETSERVICES", data);
            console.log(`Deleted ${entity} - ${status}`);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    makeBooking({ commit }, payload) {
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        return axios
          .post("booking.php", payload)
          .then(({ data }) => {
            console.log("Booking dispatched");
            console.log(data);
            commit("SETBOOKINGS", data);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    getBookings({ commit }) {
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        return axios
          .get("booking.php")
          .then(({ data, status }) => {
            // commit("SETSERVICES", data);
            console.log("Got bookings with status " + status);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    markComplete({ commit }, payload) {
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        return axios
          .put("booking.php?type=completed", payload)
          .then(({ data, status }) => {
            console.log("Got bookings with status " + status);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    rateBooking({ commit }, payload) {
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        return axios
          .put("booking.php?type=rate", payload)
          .then(({ data, status }) => {
            // commit("SETSERVICES", data);
            console.log("set rating with status " + status);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    createStaff({ commit }, payload) {
      const names = payload.name.split(" ");
      payload.firstname = names[0];
      payload.lastname = names.slice(1).join(" ");
      console.log(payload);
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        return axios
          .post("staff.php", payload)
          .then(({ data, status }) => {
            //     // commit("SETSERVICES", data);
            console.log("Created staff with status " + status);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    updateStaff({ commit }, payload) {
      const names = payload.name.split(" ");
      payload.firstname = names[0];
      payload.lastname = names.slice(1).join(" ");
      const id = payload.staff_id;
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        return axios
          .put(`staff.php?id=${id}`, payload)
          .then(({ data, status }) => {
            console.log("updated staff with status " + status);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    createService({ commit }, payload) {
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        return axios
          .post("service.php", payload)
          .then(({ data, status }) => {
            console.log("Created service with status " + status);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    },
    updateService({ commit }, payload) {
      const id = payload.service_id;
      return new Promise((resolve, reject) => {
        commit("SETREQERR", "");
        return axios
          .put(`service.php?id=${id}`, payload)
          .then(({ data, status }) => {
            console.log("Updated service with status " + status);
            resolve(data);
          })
          .catch(err => {
            commit("SETREQERR", err);
            reject(err);
          });
      });
    }
  },
  modules: {}
});
